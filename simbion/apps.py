from django.apps import AppConfig


class SimbionConfig(AppConfig):
    name = 'simbion'
