from django.conf.urls import url
from .views import index, tabel_list_beasiswa, tabel_terima_tolak,login, daftarskema1, daftarskema2, listbeasiswa


#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^listbeasiswa', listbeasiswa, name='listbeasiswa'),
    url(r'^tabel-list-beasiswa/$', tabel_list_beasiswa, name='tabel_list_beasiswa'),
    url(r'^tabel-list-beasiswa/tabel-terima-tolak/$', tabel_terima_tolak, name='tabel_terima_tolak'),
    url(r'^login/$', login, name='login'),
    url(r'^signup/$', signup, name='signup'),
    url(r'^donate/$', donate, name='donate'),
    url(r'^ind/$', ind, name='ind'),
    url(r'^yys/$', yys, name='yys'),
    url(r'^mhs/$', mhs, name='mhs'),
    url(r'^daftarskemabeasiswabaru/$', daftarskema1, name='daftarskema1'),
    url(r'^daftarskemabeasiswayangsudahada/$', daftarskema2, name='daftarskema2'),
]
