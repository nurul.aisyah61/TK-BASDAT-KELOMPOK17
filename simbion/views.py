from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def index(request):
    html = 'simbion/landingpage.html'
    return render(request, html, response)
    
def listbeasiswa(request):
    html = 'simbion/listbeasiswa.html'
    return render(request, html, response)

def form_pendaftaran_beasiswa(request):
    html = 'simbion/form_pendaftaran_beasiswa.html'
    return render(request, html, response)

def tabel_list_beasiswa(request):
    html = 'simbion/tabel_list_beasiswa.html'
    return render(request, html, response)

def tabel_terima_tolak(request):
    html = 'simbion/tabel_terima_tolak.html'
    return render(request, html, response)

def login(request):
    html = 'simbion/login_html_only.html'
    return render(request, html, response)

def daftarskema1(request):
    html = 'simbion/formskemabeasiswa-new.html'
    return render(request, html, response)

def daftarskema2(request):
    html = 'simbion/formskemabeasiswa-add.html'
    return render(request, html, response)
def signup(request):
    html = 'simbion/signup_html_only.html'
    return render(request, html, response)

def donate(request):
    html = 'simbion/donate_html_only.html'
    return render(request, html, response)

def ind(request):
    html = 'simbion/indvdonate.html'
    return render(request, html, response)

def yys(request):
    html = 'simbion/yysdonate.html'
    return render(request, html, response)
def mhs(request):
    html = 'simbion/mahasiswa.html'
    return render(request, html, response)
